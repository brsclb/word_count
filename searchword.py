import sys
import time

start_time = time.time()


def main():
    print ('-----------------------------------------------------------------------------------------------------\n')
    print ('WARNING ! THE WORD YOU WANT TO COUNT AND THE FILE, MUST BE IN THE SAME DIRECTORY WITH THE PROGRAM.\n')
    print ('-----------------------------------------------------------------------------------------------------\n')

    total_arguments = len(sys.argv) - 1

    print ("There are %s arguments that you added.\n" % total_arguments)

    count = 0
    item = 1
    found_list = []

    word_to_search = str(sys.argv[item])

    found_list.append(word_to_search)

    for item in range(2, total_arguments):
        file_path = str(sys.argv[item])
        read_list = file('%s' % file_path, 'r').read().split()

        for word in read_list:
            if word == word_to_search:
                count += 1

    print("\nLooking into all the files. Please wait for the results.. \n")
    print ('------------------------------------------------------------------------------------------------\n')

    for i in range(len(found_list)):
        print found_list[i] + ' : ' + str(count)

    print ('\n\t\t -THE END- \t\t\t\n')
    print ('------------------------------------------------------------------------------------------------\n')


main()
print("--- %s seconds ---" % (time.time() - start_time))
